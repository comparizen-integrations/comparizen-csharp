# Developer info

## Publish new version to NuGet

1. Make sure version in `Comparizen.Client/Comparizen.Client.csproj` is correct. Tag (and push) latest commit with the version currently mentioned that `.csproj` file.
2. Update `CHANGELOG.md`.
3. Run `dotnet build -c Release` and `dotnet pack -c Release`
5. From the `Comparizen.Client/bin/Release` directory, run `dotnet nuget push Comparizen.Client.<version>.nupkg --api-key <the api key> --source https://api.nuget.org/v3/index.json`
6. After pushing the publication, it can take about an hour before it becomes visible on NuGet. See [https://www.nuget.org/packages/Comparizen.Client/] for details.
6. Increase version in the aformentioned `.csproj` file and commit that to `main` branch.
7. Important: bask in the glory of a new release.

