# Comparizen C# Client

A C# / .NET client library for [Comparizen](https://comparizen.com).

## Getting started

Before using this library, make sure you're familiar with Comparizen's core concepts. 
Consult the documentation [here](https://app.comparizen.com/docs). 

### 1. Add dependency
First, add the library to your project using [NuGet](https://www.nuget.org/packages/Comparizen.Client):

`dotnet add package Comparizen.Client` or `nuget install Comparizen.Client`

### 2. Initialize the client

Initialize the client:

```csharp
var client = new ComparizenClient("<your Comparizen project's API key>")
```
You can find the API key in your Comparizen project's settings.

Note: make sure not to commit this API key to any code repository. It is recommended
to pass the API key using a environment variable (eg `System.Environment.GetEnvironmentVariable("COMPARIZEN_API_KEY")`) 
or use a configuration file like [dotenv.net](https://github.com/bolorundurowb/dotenv.net). If you don't like to use 
a library for that, [here's a blog post](https://dusted.codes/dotenv-in-dotnet) explaining on how to do without any additional dependencies.

### 3. Send screenshots to Comparizen

First, create a test-run and obtain the test-run's ID.
```csharp
var testRunId = client.CreateTestRun("<your project's ID>", "my optional testrun name");
```
Note: you can find your Comparizen project's ID in the project's settings.

Then, start uploading screenshots to that testrun:
```csharp
client.CreateComparison(testRunId, "some screenshot name",  System.IO.File.ReadAllBytes(@"my-image.png"));
```

After all screenshots have been sent, finalize the test-run:
```csharp
client.FinalizeTestRun(testRunId);
```

[A full example can be found here](https://gitlab.com/comparizen-integrations/comparizen-csharp/-/blob/main/Comparizen.Client.Test/IntegrationTest.cs). 

### 4. Waiting for results (optional)

After uploading an image to Comparizen, it will be processed and compared to a previous image that's part of the base-line (if any).
Depending on the image's size, this can take a few seconds. ComparizenClient offers some helpful methods to wait until processing is finished.

Depending on your use-case you'll either be waiting for a single comparison to be processed:
```csharp
var comparisonStatus = await comparizen.WaitForComparisonResult(comparisonId);
```

.. or wait until all the images sent to the test-run are finished:
```csharp
var comparisonStatus = await comparizen.WaitForTestRunResult(testRunId);
```
.. or combine finalizing and waiting in one call:
```csharp
var comparisonStatus = await comparizen.FinalizeTestRunAndWaitForResult(testRunId);
```

Consult the documentation [here](https://app.comparizen.com/docs) for more information on
the status results.




