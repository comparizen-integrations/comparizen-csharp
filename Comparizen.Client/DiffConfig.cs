using System;

namespace Comparizen.Client;

public class DiffConfig
{
    public static readonly DiffConfig VisibleForHumanEyes = new()
    {
        PerPixelThreshold = 1.0m,
        TotalDiffPercentageThreshold = 0.0m
    };
    public static readonly DiffConfig Strict = new()
    {
        PerPixelThreshold = 0.0m,
        TotalDiffPercentageThreshold = 0.0m
    };

    /// <summary>
    /// When comparing 2 images, this value determines how much a single pixel must be different for it to be marked as 'not matching'. 
    /// The value here can be between 0 and 100, where a value.. 
    /// <list type="bullet">
    ///     <item>
    ///         <description>between 0-1 is not visible by the human eye</description>
    ///     </item>
    ///     <item>
    ///         <description>between 1-2 is perceptible through close observation</description>
    ///     </item>
    ///     <item>
    ///         <description>between 2-10 perceptible at a glance</description>
    ///     </item>
    ///     <item>
    ///         <description>between 11 - 49 colors are more similar than opposite</description>
    ///     </item>
    ///     <item>
    ///         <description>of 100 is the exact color opposite (ie. black and white).</description>
    ///     </item>
    /// </list>
    ///     
    /// Leaving this value empty will cause the comparison to use defaults set in the project's configuration.
    /// </summary>
    public decimal? PerPixelThreshold { get; set; } = 1.0m;

    /// <summary>
    ///     When comparing 2 images, this value determines what minimum percent of the image's total amount of pixels should be marked as different before marking the images as 'not matching'.
    ///     <list type="bullet">
    ///         <item>
    ///            <description>A value of 0 means: one single pixel-difference will mark the images as 'not matching'. </description>
    ///         </item>
    ///          <item>
    ///             <description>A value of 50 will cause images to be 'not matching' only if more than 50% of the image's pixels are marked as different.</description>
    ///         </item>
    ///     </list>
    ///     Leaving this value empty will cause the comparison to use defaults set in the project's configuration.
    /// </summary>
    public decimal? TotalDiffPercentageThreshold { get; set; } = 0.0m;

    /// <summary>
    /// List of strings that acts as additional metadata to the comparison. You can use this to easily find comparisons with certain tags in Comparizen's test run overview.
    /// </summary>
    public string[] Tags { get; set; } = Array.Empty<string>();

    /// <summary>
    /// Works the same as regular Tags, but also uniquely identifies this image in the baseline in addition to the given name. 
    /// </summary>
    public string[] IdTags { get; set; } = Array.Empty<string>();
}