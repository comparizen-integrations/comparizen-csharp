namespace Comparizen.Client;

public interface IComparisonStatus
{
    public string Status { get; }

    public string Url { get; }
}