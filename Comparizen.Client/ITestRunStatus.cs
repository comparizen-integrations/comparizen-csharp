namespace Comparizen.Client;

public interface ITestRunStatus
{
    public string Status { get; }

    public string Url { get; }
}