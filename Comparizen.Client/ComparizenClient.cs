﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Comparizen.Client;

public class ComparizenClient
{
    const string ExceptionMessage = "An error occurred while communicating with the Comparizen server. Make sure you're using the latest version of the Comparizen C# client and given it a valid API key and project ID. If you are, you might experiencing an network connection issue or Comparizen server might be down temporarily.";

    private readonly TimeSpan POLLING_INTERVAL = TimeSpan.FromMilliseconds(500);

    private readonly string apiKey;
    private readonly ComparizenClientOptions options;

    private readonly HttpClient httpClient = new();

    public ComparizenClient(string apiKey, ComparizenClientOptions? options = null)
    {
        this.apiKey = apiKey;
        this.options = options ?? new ComparizenClientOptions();

        httpClient.BaseAddress = new Uri(this.options.ApiBaseUrl);
        httpClient.Timeout = this.options.Timeout;
    }

    public async Task<String> CreateTestRun(string projectId, string? name = null)
    {
        return await WithErrorHandling(async () =>
        {
            var postBody = new Dictionary<string, string>
            {
                ["apiKey"] = apiKey,
                ["projectId"] = projectId
            };

            if (name != null)
            {
                postBody["name"] = name;
            }

            HttpResponseMessage response = await httpClient.PostAsJsonAsync(
                    "rest/testrun", postBody);

            var TestRunResponse = await ExtractFromJsonResponse<CreateTestRunResponse>(response);
            return TestRunResponse.Id!;
        });
    }

    public async Task<String> CreateComparison(string testRunId, string name, string base64EncodedImageData, DiffConfig? diffConfig = null)
    {
        return await this.CreateComparison(testRunId, name, Convert.FromBase64String(base64EncodedImageData), diffConfig);
    }

    public async Task<String> CreateComparison(string testRunId, string name, byte[] imageData, DiffConfig? diffConfig = null)
    {
        using var formData = new MultipartFormDataContent();
        using var fileContent = new ByteArrayContent(imageData);
        formData.Add(new StringContent(apiKey), "apiKey");
        formData.Add(new StringContent(name), "name");
        formData.Add(new StringContent(testRunId), "testRunId");

        if (diffConfig != null)
        {
#pragma warning disable CS8604 // Possible null reference argument.
            if (diffConfig.PerPixelThreshold != null)
            {
                formData.Add(new StringContent(diffConfig.PerPixelThreshold.Value.ToString("##0.###", CultureInfo.InvariantCulture)), "perPixelThreshold");
            }

            if (diffConfig.TotalDiffPercentageThreshold != null)
            {
                formData.Add(new StringContent(diffConfig.TotalDiffPercentageThreshold.Value.ToString("##0.###", CultureInfo.InvariantCulture)), "totalDiffPercentageThreshold");
            }
#pragma warning restore CS8604 // Possible null reference argument.

            if (diffConfig.IdTags != null && diffConfig.IdTags.Length > 0)
            {
                formData.Add(new StringContent(string.Join(",", diffConfig.IdTags)), "idTags");
            }

            if (diffConfig.Tags != null && diffConfig.Tags.Length > 0)
            {
                formData.Add(new StringContent(string.Join(",", diffConfig.Tags)), "tags");
            }
        }

        formData.Add(fileContent, "file", "image.png");

        return await WithErrorHandling(async () =>
        {
            var response = await httpClient.PostAsync("/rest/comparison", formData);
            var ComparisonResponse = await ExtractFromJsonResponse<CreateComparisonResponse>(response);
            return ComparisonResponse.ComparisonId!;
        });
    }

    public async Task FinalizeTestRun(string testRunId)
    {
        var postBody = new Dictionary<string, string>
        {
            ["apiKey"] = apiKey
        };

        await WithErrorHandling(async () =>
        {
            var response = await httpClient.PostAsJsonAsync($"/rest/testrun/{testRunId}/finalize", postBody);
            await EnsureSuccessfulHttpResponse(response);
            return 1;
        });
    }

    public async Task<ITestRunStatus> FinalizeTestRunAndWaitForResult(string testRunId)
    {
        await this.FinalizeTestRun(testRunId);
        return await this.WaitForTestRunResult(testRunId);
    }

    public async Task<ITestRunStatus> GetTestRunStatus(string testRunId)
    {
        return await WithErrorHandling(async () =>
        {
            var queryParams = new Dictionary<string, string>
            {
                ["apiKey"] = apiKey
            };

            var queryString = await new FormUrlEncodedContent(queryParams).ReadAsStringAsync();

            HttpResponseMessage response = await httpClient.GetAsync($"rest/testrun/{testRunId}/status?{queryString}");
            return await ExtractFromJsonResponse<GetTestRunStatusResponse>(response);

        });
    }

    public async Task<ITestRunStatus> WaitForTestRunResult(string testRunId, TimeSpan? maxPollingTime = null)
    {
        Task<ITestRunStatus> pollFn() { return this.GetTestRunStatus(testRunId); }
        bool verifyFn(ITestRunStatus status) { return status.Status != "PROCESSING"; }

        if (maxPollingTime == null)
        {
            maxPollingTime = TimeSpan.FromSeconds(20);
        }

        return await PollWithTimeoutAsync(pollFn, verifyFn, POLLING_INTERVAL, maxPollingTime);

    }

    public async Task<IComparisonStatus> GetComparisonStatus(string comparisonId)
    {
        return await WithErrorHandling(async () =>
        {
            var queryParams = new Dictionary<string, string>
            {
                ["apiKey"] = apiKey
            };

            var queryString = await new FormUrlEncodedContent(queryParams).ReadAsStringAsync();

            HttpResponseMessage response = await httpClient.GetAsync($"rest/comparison/{comparisonId}/status?{queryString}");
            return await ExtractFromJsonResponse<GetComparisonStatusResponse>(response);
        });
    }

    public async Task<IComparisonStatus> WaitForComparisonResult(string comparisonId, TimeSpan? maxPollingTime = null)
    {
        Task<IComparisonStatus> pollFn() { return this.GetComparisonStatus(comparisonId); }
        bool verifyFn(IComparisonStatus status) { return status.Status != "UNPROCESSED"; }

        if (maxPollingTime == null)
        {
            maxPollingTime = TimeSpan.FromSeconds(20);
        }

        return await PollWithTimeoutAsync(pollFn, verifyFn, POLLING_INTERVAL, maxPollingTime);
    }

    private static async Task<T> ExtractFromJsonResponse<T>(HttpResponseMessage response) where T : IValidatable
    {
        return await WithErrorHandling(async () =>
        {
            await EnsureSuccessfulHttpResponse(response);

            T? extracted = await response.Content!.ReadFromJsonAsync<T>() ?? throw new WebException("Comparizen server responded with an empty response.");
            if (!extracted.IsValid())
            {
                throw new WebException("Comparizen server responded with an invalid response.");
            }

            return extracted;
        });

    }

    private static async Task EnsureSuccessfulHttpResponse(HttpResponseMessage response)
    {
        if (response is null)
        {
            throw new WebException("Received a null response from Comparizen server.");
        }

        if (!response.IsSuccessStatusCode)
        {
            if (response.StatusCode == HttpStatusCode.Unauthorized || response.StatusCode == HttpStatusCode.Forbidden)
            {
                throw new ComparizenClientException("Comparizen server rejected your request due to invalid or missing authorization. Please check if you're using a valid API Key and/or Comparizen Project ID.");
            }

            var body = await response.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(body))
            {
                throw new WebException($"Received an empty error response with HTTP status code {(int)response.StatusCode} from Comparizen server.");
            }
            else
            {
                throw new WebException($"Received an error response with HTTP status code {(int)response.StatusCode} from Comparizen server. Details: {body}");
            }
        }
    }

    private static async Task<T> PollWithTimeoutAsync<T>(Func<Task<T>> pollFn, Func<T, bool> verifyFn, TimeSpan interval, TimeSpan? maxPollingTime = null)
    {
        DateTime startTime = DateTime.Now;
        if (maxPollingTime == null)
        {
            maxPollingTime = TimeSpan.FromSeconds(20);
        }

        Exception? lastException = null;

        while (DateTime.Now - startTime < maxPollingTime)
        {
            try
            {
                T result = await pollFn();
                if (verifyFn(result))
                {
                    return result;
                }
                lastException = null;
            }
            catch(Exception e)
            {
                lastException = e;
            }

            await Task.Delay(interval);
        }

        if (lastException == null) {
            throw new ComparizenClientException($"Comparizen server was not finished processing your image(s) within {maxPollingTime.Value.Seconds} seconds. It might need more time.");
        } else {
            throw new ComparizenClientException($"Comparizen server was not finished processing your image(s) within {maxPollingTime.Value.Seconds} seconds. Last attempt reported an error: {lastException.Message}.", lastException);
        }
    }

    private static async Task<T> WithErrorHandling<T>(Func<Task<T>> fn)
    {
        try
        {
            return await fn();
        }
        catch (Exception ex)
        {
            if (ex.GetType() == typeof(ComparizenClientException))
            {
                throw;
            }
            throw new ComparizenClientException(ExceptionMessage, ex);
        }
    }

    private class CreateTestRunResponse : IValidatable
    {
        public string? Id { get; set; }

        public Boolean IsValid()
        {
            return !string.IsNullOrEmpty(this.Id);
        }
    }

    private class CreateComparisonResponse : IValidatable
    {
        public string? ComparisonId { get; set; }

        public Boolean IsValid()
        {
            return !string.IsNullOrEmpty(this.ComparisonId);
        }
    }

    private class GetTestRunStatusResponse : ITestRunStatus, IValidatable
    {
        public string? Status { get; set; }
        public string? Url { get; set; }

        public Boolean IsValid()
        {
            return !string.IsNullOrEmpty(this.Status) && !string.IsNullOrEmpty(this.Url);
        }
    }

    private class GetComparisonStatusResponse : IComparisonStatus, IValidatable
    {
        public string? Status { get; set; }
        public string? Url { get; set; }

        public Boolean IsValid()
        {
            return !string.IsNullOrEmpty(this.Status) && !string.IsNullOrEmpty(this.Url);
        }
    }

    private interface IValidatable
    {
        public Boolean IsValid();
    }
}