using System;

namespace Comparizen.Client;

public class ComparizenClientOptions
{
    public string ApiBaseUrl { get; set; } = "https://app.comparizen.com";
    public TimeSpan Timeout { get; set; } = new TimeSpan(0, 0, 60);
}