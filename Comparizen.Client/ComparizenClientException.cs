using System;

namespace Comparizen.Client
{
    public class ComparizenClientException : Exception
    {
        public ComparizenClientException(string message) : base(message)
        {
        }

        public ComparizenClientException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}