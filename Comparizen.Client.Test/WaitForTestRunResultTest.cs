using System.Text.Json;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;

namespace Comparizen.Client.Test;

[TestClass]
public class WaitForTestRunResultTest
{

    private static WireMockServer mockServer = null!;

    private static ComparizenClient comparizenClient = null!;

    private static readonly string Path = "/rest/testrun/some-testrun-id/status";

    private static readonly string ProcessingResponse = "PROCESSING";

    [ClassInitialize]
    public static void PrepareClass(TestContext context)
    {
        mockServer = WireMockServer.Start();
        var options = new ComparizenClientOptions();
        options.ApiBaseUrl = $"http://localhost:{mockServer.Port}";

        comparizenClient = new ComparizenClient("some-api-key", options);
    }

    [TestInitialize]
    public void Setup() {
        mockServer.Reset();
    }

    [TestMethod]
    public async Task TestWaitForProcessingFinished()
    {
        var scenarioName = "WaitForProcessingFinished";
        mockServer
            .Given(Request.Create().WithPath(Path).UsingGet())
            .InScenario(scenarioName)
            .WillSetStateTo("1")
            .RespondWith(
                Response.Create()
                .WithStatusCode(200)
                .WithBody(CreateBody(ProcessingResponse))
            );

        mockServer
           .Given(Request.Create().WithPath(Path).UsingGet())
           .InScenario(scenarioName)
           .WhenStateIs("1")
           .WillSetStateTo("2")
           .RespondWith(
               Response.Create()
               .WithStatusCode(200)
               .WithBody(CreateBody(ProcessingResponse))
           );

        mockServer
            .Given(Request.Create().WithPath(Path).UsingGet())
            .InScenario(scenarioName)
            .WhenStateIs("2")
            .RespondWith(
                Response.Create()
                .WithStatusCode(200)
                .WithBody(CreateBody("ALL_OK"))
            );

        var result = await comparizenClient.WaitForTestRunResult("some-testrun-id");
        Assert.AreEqual("ALL_OK", result.Status);
        Assert.AreEqual("http://some-url", result.Url);
    }

    [TestMethod]
    public async Task TestWaitForProcessingNeverFinishes()
    {
        mockServer
            .Given(Request.Create().WithPath(Path).UsingGet())
            .RespondWith(
                Response.Create()
                .WithStatusCode(200)
                .WithBody(CreateBody(ProcessingResponse))
            );

        var ex = await Assert.ThrowsExceptionAsync<ComparizenClientException>(() =>
            comparizenClient.WaitForTestRunResult("some-testrun-id", new TimeSpan(0, 0, 3))
        );
        StringAssert.StartsWith(ex.Message, "Comparizen server was not finished processing your image(s) within 3 seconds");
    }

    [TestMethod]
    public async Task TestWaitForProcessingWithInBetweenErrors()
    {
        var scenarioName = "WaitForProcessingFinished";
        mockServer
            .Given(Request.Create().WithPath(Path).UsingGet())
            .InScenario(scenarioName)
            .WillSetStateTo("1")
            .RespondWith(
                Response.Create()
                .WithStatusCode(200)
                .WithBody(CreateBody(ProcessingResponse))
            );

        mockServer
           .Given(Request.Create().WithPath(Path).UsingGet())
           .InScenario(scenarioName)
           .WhenStateIs("1")
           .WillSetStateTo("2")
           .RespondWith(
               Response.Create()
               .WithStatusCode(500)
           );

        mockServer
            .Given(Request.Create().WithPath(Path).UsingGet())
            .InScenario(scenarioName)
            .WhenStateIs("2")
            .RespondWith(
                Response.Create()
                .WithStatusCode(200)
                .WithBody(CreateBody("ALL_OK"))
            );

        var result = await comparizenClient.WaitForTestRunResult("some-testrun-id");
        Assert.AreEqual("ALL_OK", result.Status);
        Assert.AreEqual("http://some-url", result.Url);
    }

    [TestMethod]
    public async Task TestWaitForProcessingAllErrors()
    {
       mockServer
            .Given(Request.Create().WithPath(Path).UsingGet())
            .RespondWith(
                Response.Create()
                .WithStatusCode(500)
            );

        var ex = await Assert.ThrowsExceptionAsync<ComparizenClientException>(() =>
            comparizenClient.WaitForTestRunResult("some-testrun-id", new TimeSpan(0, 0, 3))
        );
        StringAssert.StartsWith(ex.Message, "Comparizen server was not finished processing your image(s) within 3 seconds. Last attempt reported an error");
    }

    private static string CreateBody(string Status)
    {
        return JsonSerializer.Serialize(new Dictionary<string, string> {
                    { "status", Status },
                    { "url", "http://some-url" }
                });
    }
}