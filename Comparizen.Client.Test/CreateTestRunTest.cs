using System.Text.Json;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;

namespace Comparizen.Client.Test;

[TestClass]
public class CreateTestRunTest
{

    private static WireMockServer mockServer = null!;

    private static ComparizenClient comparizenClient = null!;

    [ClassInitialize]
    public static void PrepareClass(TestContext context)
    {
        mockServer = WireMockServer.Start();
        var options = new ComparizenClientOptions();
        options.ApiBaseUrl = $"http://localhost:{mockServer.Port}";

        comparizenClient = new ComparizenClient("some-api-key", options);
    }

    [TestMethod]
    public async Task TestSuccess()
    {
        mockServer
            .Given(Request.Create().WithPath("/rest/testrun").UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(200)
                .WithBody(JsonSerializer.Serialize(new Dictionary<string, string> { { "id", "some-testrun-id" } }))
            );

        var testRunId = await comparizenClient.CreateTestRun("some-project-id");

        Assert.AreEqual("some-testrun-id", testRunId);
    }

    [TestMethod]
    public async Task TestUnauthorized()
    {
        mockServer
            .Given(Request.Create().WithPath("/rest/testrun").UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(401)
            );

        var ex = await Assert.ThrowsExceptionAsync<ComparizenClientException>(() =>
            comparizenClient.CreateTestRun("some-project-id")
        );
        StringAssert.StartsWith(ex.Message, "Comparizen server rejected your request due to invalid or missing authorization");
    }

    [TestMethod]
    public async Task TestForbidden()
    {
        mockServer
            .Given(Request.Create().WithPath("/rest/testrun").UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(403)
            );

        var ex = await Assert.ThrowsExceptionAsync<ComparizenClientException>(() =>
            comparizenClient.CreateTestRun("some-project-id")
        );
        StringAssert.StartsWith(ex.Message, "Comparizen server rejected your request due to invalid or missing authorization");
    }


    [TestMethod]
    public async Task TestBadRequest()
    {
        mockServer
            .Given(Request.Create().WithPath("/rest/testrun").UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(400)
                .WithBody("some details")
            );

        var ex = await Assert.ThrowsExceptionAsync<ComparizenClientException>(() =>
            comparizenClient.CreateTestRun("some-project-id")
        );
        StringAssert.StartsWith(ex.InnerException!.Message, "Received an error response with HTTP status code 400 from Comparizen server. Details: some details", ex.InnerException!.Message);
    }

    [TestMethod]
    public async Task TestErrorNoBody()
    {
        mockServer
            .Given(Request.Create().WithPath("/rest/testrun").UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(500)
            );

        var ex = await Assert.ThrowsExceptionAsync<ComparizenClientException>(() =>
            comparizenClient.CreateTestRun("some-project-id")
        );
        StringAssert.StartsWith(ex.InnerException!.Message, "Received an empty error response with HTTP status code 500 from Comparizen server.");
    }

    [TestMethod]
    public async Task TestErrorNoValidBody()
    {
        mockServer
            .Given(Request.Create().WithPath("/rest/testrun").UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(200)
                .WithBody(JsonSerializer.Serialize(new Dictionary<string, string> { { "weirdkey", "weirdbody" } }))
            );

        var ex = await Assert.ThrowsExceptionAsync<ComparizenClientException>(() =>
            comparizenClient.CreateTestRun("some-project-id")
        );
        Assert.AreEqual("Comparizen server responded with an invalid response.", ex.InnerException!.Message);
    }
}