namespace Comparizen.Client.Test;

using System;
using System.IO;

public static class TestUtil
{
    public static void LoadDotEnvFile()
    {
        var root = Directory.GetCurrentDirectory();
        var filePath = Path.Combine(root, ".env");
        if (!File.Exists(filePath))
            return;

        foreach (var line in File.ReadAllLines(filePath))
        {
            var parts = line.Split(
                '=',
                StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length != 2)
                continue;

            Environment.SetEnvironmentVariable(parts[0], parts[1]);
        }
    }
}
