using System.Text.Json;
using System.Text.RegularExpressions;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;

namespace Comparizen.Client.Test;

[TestClass]
public class CreateComparisonTest
{

    private static WireMockServer mockServer = null!;

    private static ComparizenClient comparizenClient = null!;

    [ClassInitialize]
    public static void PrepareClass(TestContext context)
    {
        mockServer = WireMockServer.Start();
        var options = new ComparizenClientOptions
        {
            ApiBaseUrl = $"http://localhost:{mockServer.Port}"
        };

        comparizenClient = new ComparizenClient("some-api-key", options);
    }

    [TestMethod]
    public async Task TestSuccess()
    {
        mockServer
            .Given(Request.Create()
            .WithPath("/rest/comparison")
            .UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(200)
                .WithBody(JsonSerializer.Serialize(new Dictionary<string, string> { { "comparisonId", "some-comparison-id" } }))
            );

        var comparisonId = await comparizenClient.CreateComparison("some-testrun-id", "some name", GetImageData(), new DiffConfig
        {
            IdTags = new string[] { "id-tag 1", "id-tag 2" },
            Tags = new string[] { "tag 1", "tag 2" },
            PerPixelThreshold = 0.5m,
            TotalDiffPercentageThreshold = 1.0m
        });

        Assert.AreEqual("some-comparison-id", comparisonId);

        var bodyString = getBodyStringFromLastRequest(mockServer.FindLogEntries(
            Request.Create().WithPath("/rest/comparison*").UsingPost()
        ));

        StringAssert.Contains(bodyString, "name=testRunId\r\n\r\nsome-testrun-id");
        StringAssert.Contains(bodyString, "name=name\r\n\r\nsome name");
        StringAssert.Contains(bodyString, "name=tags\r\n\r\ntag 1,tag 2");
        StringAssert.Contains(bodyString, "name=idTags\r\n\r\nid-tag 1,id-tag 2");
        StringAssert.Contains(bodyString, "name=perPixelThreshold\r\n\r\n0.5");
        StringAssert.Contains(bodyString, "name=totalDiffPercentageThreshold\r\n\r\n1");
    }

    [TestMethod]
    public async Task TestSuccessDefaultDiffConfig()
    {
        mockServer
            .Given(Request.Create()
            .WithPath("/rest/comparison")
            .UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(200)
                .WithBody(JsonSerializer.Serialize(new Dictionary<string, string> { { "comparisonId", "some-comparison-id" } }))
            );

        var comparisonId = await comparizenClient.CreateComparison("some-testrun-id", "some name", GetImageData());

        Assert.AreEqual("some-comparison-id", comparisonId);

        var bodyString = getBodyStringFromLastRequest(mockServer.FindLogEntries(
            Request.Create().WithPath("/rest/comparison*").UsingPost()
        ));

        StringAssert.Contains(bodyString, "name=testRunId\r\n\r\nsome-testrun-id");
        StringAssert.Contains(bodyString, "name=name\r\n\r\nsome name");
        StringAssert.DoesNotMatch(bodyString, new Regex(".*tags.*"));
        StringAssert.DoesNotMatch(bodyString, new Regex(".*idTags.*"));
        StringAssert.DoesNotMatch(bodyString, new Regex(".*perPixelThreshold.*"));
        StringAssert.DoesNotMatch(bodyString, new Regex(".*totalDiffPercentageThreshold.*"));
    }

    [TestMethod]
    public async Task TestUnauthorized()
    {
        mockServer
            .Given(Request.Create().WithPath("/rest/comparison").UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(401)
            );
        
        var ex = await Assert.ThrowsExceptionAsync<ComparizenClientException>(() =>
            comparizenClient.CreateComparison("some-testrun-id", "some name", GetImageData())
        );
        StringAssert.StartsWith(ex.Message, "Comparizen server rejected your request due to invalid or missing authorization");
    }


    [TestMethod]
    public async Task TestForbidden()
    {
        mockServer
            .Given(Request.Create().WithPath("/rest/comparison").UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(403)
            );

        var ex = await Assert.ThrowsExceptionAsync<ComparizenClientException>(() =>
            comparizenClient.CreateComparison("some-testrun-id", "some name", GetImageData())
        );
        StringAssert.StartsWith(ex.Message, "Comparizen server rejected your request due to invalid or missing authorization");
    }


    [TestMethod]
    public async Task TestBadRequest()
    {
        mockServer
            .Given(Request.Create().WithPath("/rest/comparison").UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(400)
                .WithBody("some details")
            );

        var ex = await Assert.ThrowsExceptionAsync<ComparizenClientException>(() =>
            comparizenClient.CreateComparison("some-testrun-id", "some name", GetImageData())
        );
        StringAssert.StartsWith(ex.InnerException!.Message, "Received an error response with HTTP status code 400 from Comparizen server. Details: some details", ex.InnerException!.Message);
    }

    [TestMethod]
    public async Task TestErrorNoBody()
    {
        mockServer
            .Given(Request.Create().WithPath("/rest/comparison").UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(500)
            );

        var ex = await Assert.ThrowsExceptionAsync<ComparizenClientException>(() =>
            comparizenClient.CreateComparison("some-testrun-id", "some name", GetImageData())
        );
        StringAssert.StartsWith(ex.InnerException!.Message, "Received an empty error response with HTTP status code 500 from Comparizen server.");
    }

    [TestMethod]
    public async Task TestErrorNoValidBody()
    {
        mockServer
            .Given(Request.Create().WithPath("/rest/comparison").UsingPost())
            .RespondWith(
                Response.Create()
                .WithStatusCode(200)
                .WithBody(JsonSerializer.Serialize(new Dictionary<string, string> { { "weirdkey", "weirdbody" } }))
            );

        var ex = await Assert.ThrowsExceptionAsync<ComparizenClientException>(() =>
            comparizenClient.CreateComparison("some-testrun-id", "some name", GetImageData())
        );
        Assert.AreEqual("Comparizen server responded with an invalid response.", ex.InnerException!.Message);
    }

    private static byte[] GetImageData()
    {
        return System.IO.File.ReadAllBytes(@"../../../TestImages/test-image-1.png");
    }

    private static string getBodyStringFromLastRequest(IEnumerable<WireMock.Logging.LogEntry> logEntries) 
    {
        var bytes = logEntries.Last().RequestMessage.BodyData!.BodyAsBytes;
        return System.Text.Encoding.Default.GetString(bytes!);
    }
}