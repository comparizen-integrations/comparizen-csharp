namespace Comparizen.Client.Test;

[TestClass]
public class IntegrationTest
{
    [TestMethod]
    // this test assumes that theres a 'test image 1' in the baseline, based on test-image-1.png
    public async Task TestImageUploadMinimal()
    {
        byte[] imageData = System.IO.File.ReadAllBytes(@"../../../TestImages/test-image-2.png");;

        TestUtil.LoadDotEnvFile();
        var apiKey = System.Environment.GetEnvironmentVariable("COMPARIZEN_API_KEY")!;
        var projectId = System.Environment.GetEnvironmentVariable("COMPARIZEN_PROJECT_ID")!;

        var comparizen = new ComparizenClient(apiKey);
        var testRunId = await comparizen.CreateTestRun(projectId, "some name");

        Assert.IsNotNull(testRunId);

        var comparisonId = await comparizen.CreateComparison(testRunId, "test image 1", imageData);
        Assert.IsNotNull(comparisonId);

        var comparisonStatus = await comparizen.WaitForComparisonResult(comparisonId);
        Assert.AreEqual("DIFF_FOUND", comparisonStatus.Status);
        StringAssert.StartsWith(comparisonStatus.Url, "https://app.comparizen.com/");

        await comparizen.FinalizeTestRun(testRunId);

        var result = await comparizen.WaitForTestRunResult(testRunId);
        Assert.AreEqual("DIFFS_FOUND", result.Status);
        StringAssert.StartsWith(result.Url, "https://app.comparizen.com/");
    }

    [TestMethod]
    // this test assumes that theres a 'test image 1' in the baseline, based on test-image-1.png, but without ID tags so this new upload is considered 'new'.
    public async Task TestImageUploadAllOptions()
    {
        byte[] imageData = System.IO.File.ReadAllBytes(@"../../../TestImages/test-image-2.png");

        TestUtil.LoadDotEnvFile();
        var apiKey = System.Environment.GetEnvironmentVariable("COMPARIZEN_API_KEY")!;
        var projectId = System.Environment.GetEnvironmentVariable("COMPARIZEN_PROJECT_ID")!;

        var comparizen = new ComparizenClient(apiKey);
        var testRunId = await comparizen.CreateTestRun(projectId, "some name");

        Assert.IsNotNull(testRunId);

        var comparisonId = await comparizen.CreateComparison(testRunId, "test image 1", imageData, new DiffConfig {
            IdTags=new string[]{"some id tag"},
            Tags=new string[]{"some regular tag"},
            PerPixelThreshold=1.0m,
            TotalDiffPercentageThreshold=1.0m
        });
        Assert.IsNotNull(comparisonId);

        var comparisonStatus = await comparizen.WaitForComparisonResult(comparisonId);
        Assert.AreEqual("NEW_TO_BASELINE", comparisonStatus.Status);
        StringAssert.StartsWith(comparisonStatus.Url, "https://app.comparizen.com/");

        await comparizen.FinalizeTestRun(testRunId);

        var result = await comparizen.WaitForTestRunResult(testRunId);
        Assert.AreEqual("DIFFS_FOUND", result.Status);
        StringAssert.StartsWith(result.Url, "https://app.comparizen.com/");
    }
}